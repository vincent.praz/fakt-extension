var successURL = 'https://www.facebook.com/connect/login_success.html';

function onFacebookLogin() {
    // if (!localStorage.accessToken) {
    chrome.tabs.getAllInWindow(null, function (tabs) {
        for (var i = 0; i < tabs.length; i++) {
            if (tabs[i].url.indexOf(successURL) == 0) {
                var params = tabs[i].url.split('#')[1];
                access = params.split('&')[0]
                console.log(access);
                localStorage.accessToken = access;
                chrome.tabs.onUpdated.removeListener(onFacebookLogin);
                return;
            }
        }
    });
    // }
}

chrome.tabs.onUpdated.addListener(onFacebookLogin);


// Warn about it button
document.getElementById("warnthemButton").addEventListener("click", function () {
    chrome.storage.sync.get(['reportedLinks'], function (result) {
        if (chrome.runtime.error || !result.reportedLinks) {
            // Initialize the empty list
            let linkList = [];
            // Push the post Id into the list
            linkList.push(document.getElementById('postId').innerText);

            //Sync the chrome storage
            chrome.storage.sync.set({"reportedLinks": linkList}, function () {
                console.log('Value is set to ' + linkList);
            });
            document.getElementById('reportedLinksNumber').innerText = 1;
        } else {
            // Increase counter by 1
            // let counter =  result.reportedLinks + 1;
            console.log(result.reportedLinks);
            let linkList = result.reportedLinks;
            linkList.push(document.getElementById('postId').innerText);
            chrome.storage.sync.set({"reportedLinks": linkList}, function () {
                console.log('Value is set to ' + linkList);
            });

            // Update the counter on the screen
            document.getElementById('reportedLinksNumber').innerText = linkList.length - 1;
        }
    });

    // Remove post thing and put success message instead
    document.getElementById('post').classList.add('hidden');
    document.getElementById('successPost').classList.remove('hidden');
});


// Warn them all about it button
document.getElementById("warnthemAllButton").addEventListener("click", function () {
    chrome.storage.sync.get(['reportedLinks'], function (result) {
        // Retrieves all the ids of the child img of imgDiv
        let postIdLinks = [];
        $("#imgDiv > img").each((index, elem) => {
            postIdLinks.push(elem.id);
        });

        // Adds all the ids of the posts in the reported links list
        let linkList = result.reportedLinks.concat(postIdLinks);

        chrome.storage.sync.set({"reportedLinks": linkList}, function () {
            console.log('Value is set to ' + linkList);
        });

        // Update the counter on the screen
        document.getElementById('reportedLinksNumber').innerText = linkList.length - 1;
    });


    // Remove post thing and put success message instead
    document.getElementById('postImages').classList.add('hidden');
    document.getElementById('successPostImages').classList.remove('hidden');
});


document.getElementById("continue").addEventListener("click", function () {
    console.log("Refresh");
    location.reload(true);
});

document.getElementById("continueImages").addEventListener("click", function () {
    console.log("Refresh");
    location.reload(true);
});