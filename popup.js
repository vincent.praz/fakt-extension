// Extracts domain name + extension from an url
function domain_from_url(url) {
    var result
    var match
    if (match = url.match(/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n\?\=]+)/im)) {
        result = match[1]
        if (match = result.match(/^[^\.]+\.(.+\..+)$/)) {
            result = match[1]
        }
    }
    return result
}

// Display retrieves infos about the full_picture of the post
function getFullPicture(postId, callback) {
    var graphUrl = "https://graph.facebook.com/v6.0/" + postId + "/?fields=full_picture&" + localStorage.accessToken + "&callback=" + callback;

    var script = document.createElement("script");
    script.src = graphUrl;
    document.body.appendChild(script);
}

// Change the src of the postImage to display the full_picture of the post
function displayFullPicture(post) {
    document.getElementById('postImage').src = post.full_picture;
}

// Add Post images to the post images div
function displayFullPictureList(post) {
    // Main div in which we will ad the post images
    let imgDiv = document.getElementById('imgDiv');

    // Create an img element
    let img = document.createElement('img');
    img.src = post.full_picture;
    img.classList = "imgListThumbnails";
    img.id = post.id; // Save the id of the post in the image id
    // Adds the img to the mainDiv
    imgDiv.appendChild(img);
}


// Shows the number of reported links onload
document.body.onload = function () {
    chrome.storage.sync.get(['reportedLinks'], function (result) {
        if (chrome.runtime.error || !result.reportedLinks) {
            // Initialize the empty list
            let linkList = [];
            // Push the post Id into the list
            linkList.push(document.getElementById('postId').innerText);

            //Sync the chrome storage
            chrome.storage.sync.set({"reportedLinks": linkList}, function () {
                console.log('Value is set to ' + linkList);
            });
            document.getElementById('reportedLinksNumber').innerText = 0;
        } else {
            let linkList = result.reportedLinks;
            // Update the counter on the screen
            document.getElementById('reportedLinksNumber').innerText = linkList.length - 1;
        }
    });
};

console.log(localStorage.accessToken);
if (localStorage.accessToken) {
    // Check Facebook connection
    var graphUrl = "https://graph.facebook.com/me?fields=picture&" + localStorage.accessToken + "&callback=displayUser";

    var script = document.createElement("script");
    script.src = graphUrl;
    document.body.appendChild(script);

    //Permet de récupérer le user (id,fullname)
    function displayUser(user) {
        // Recup user group feed url
        // let graphUrl = "https://graph.facebook.com/v6.0/me/groups?fields=feed&access_token=" + localStorage.accessToken + "&callback=displayFeed"

        document.getElementById('profilePicture').src = user.picture.data.url;
        document.getElementById('profilePictureSuccess').src = user.picture.data.url;
        document.getElementById('profilePictureSuccessImages').src = user.picture.data.url;

        //TODO: Amélioration possible : Get group ID 1st de façon pas en dure Group id : 1123754267990200

        // URL To the graph to get the feed of the group
        var graphUrl = "https://graph.facebook.com/v6.0/1123754267990200/feed/?fields=link&" + localStorage.accessToken + "&callback=displayFeed";

        console.log(graphUrl);

        var script = document.createElement("script");
        script.src = graphUrl;
        document.body.appendChild(script);

    }

    // Once the user is connected and we got his feed
    // We search for links contained in his feed
    function displayFeed(feed) {
        //If user connected we display counter and hide the connection message
        document.getElementById('counterSection').classList.remove('hidden');
        document.getElementById('connect').classList.add('hidden');


        // Dangerous links counter initiation
        let dangerousLinks = 0;

        // Foreach post
        if (feed.data) {
            let postList = [];
            chrome.storage.sync.get(['reportedLinks'], function (result) {
                postList = result.reportedLinks;
                console.log(postList);

                console.log(postList);
                feed.data.forEach(function myFunction(post, index) {
                        // Check if post has a link inside AND if the post id isn't in the already reported postList
                        if (post.link && postList.indexOf(post.id) === -1) {
                            // Format the post.link to be domain_name.extension only for comparison
                            let link = domain_from_url(post.link);

                            // Comparing the link with the list of known fake news websites
                            $.getJSON("sources/sources.json", function (data) {
                                $.each(data, function (key, value) {
                                    // If Link is dangerous
                                    if (key === link) {
                                        // If it is the first post detected we want to display it
                                        if (dangerousLinks === 0) {
                                            // Add the Warn them about it button
                                            let button = document.createElement("BUTTON");
                                            button.textContent = "Warn them about it";

                                            // Save the post.id in the html
                                            document.getElementById('postId').innerText = post.id;

                                            // Display the link
                                            document.getElementById('link').innerHTML = "www." + link;

                                            // Display the full post block
                                            document.getElementById('post').classList.remove("hidden");

                                            // Add correct formaction to post a message to facebook group
                                            var graphUrl = "https://graph.facebook.com/v6.0/1123754267990200/feed?" + localStorage.accessToken;
                                            document.getElementById('postForm').action = graphUrl;

                                            // Remove first part of post id groupId_postId to create the Link to the post
                                            let linkToPost = post.id
                                            linkToPost = linkToPost.split("_").pop();

                                            // Add predefined message to textarea
                                            document.getElementById('comment').innerHTML = "Hey it seems like this post : https://www.facebook.com/groups/1123754267990200/" + linkToPost + "\nContains an unsafe link with unverified informations.\nFor more informations please visit \nhttps://fakt-website.000webhostapp.com/Fakt !";

                                            // Get the full picture of the post
                                            getFullPicture(post.id, "displayFullPicture");
                                        } else if (dangerousLinks < 7) { // We limit the size of the unsafe post list to 6 max
                                            // Retrieve the full picture of the post and callback to the list of picture generation
                                            // Add correct formaction to post a message to facebook group
                                            var graphUrl = "https://graph.facebook.com/v6.0/1123754267990200/feed?" + localStorage.accessToken;
                                            document.getElementById('postFormList').action = graphUrl;

                                            let linkToPost = post.id
                                            linkToPost = linkToPost.split("_").pop();

                                            // Add predefined message to textarea containing all the links
                                            document.getElementById('commentForm').innerHTML += "\nhttps://www.facebook.com/groups/1123754267990200/" + linkToPost;

                                            document.getElementById('postImages').classList.remove('hidden');

                                            getFullPicture(post.id, "displayFullPictureList");
                                        }

                                        // Increase the dangerous links counter
                                        dangerousLinks++;


                                        // We update the dangerous links number screen
                                        document.getElementById('unsafeLinksNumber').innerHTML = dangerousLinks;
                                    }
                                });
                            });
                        }
                    }
                );
            });
        } else {
            console.log("FEED UNDEFINED");
        }
    }
}
